# Little Zebra Donation Site

**Site built using Foundation 6.4


- Handlebars HTML templates with Panini
- Sass compilation and prefixing
- JavaScript module bundling with webpack
- Built-in BrowserSync server
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)


### Using the CLI

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```


The CLI will prompt you to give your project a name. The template will be downloaded into a folder with this name.

Now `cd` to your project name and to start your project run 

```bash
foundation watch
```

### Manual Setup for Little Zebra

To manually set up the template, first download it with Git:

```bash
git clone https://kyle_anotherlevel@bitbucket.org/kyle_anotherlevel/little-zebra.git
```

Then open the folder in your command line, and install the needed dependencies:

```bash
cd little-zebra
npm install
bower install
```

Finally, run `npm start` to run Gulp. Your finished site will be created in a folder called `dist`, viewable at this URL:

```
http://localhost:8000
```

To create compressed, production-ready assets, run `npm run build`.
